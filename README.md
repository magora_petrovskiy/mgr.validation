# mgr.validation

Kind of a wrapper for generic angular validation logic. Basically, what it does, it binds to a certain field (and form if needed) and shows/hide validation messages nicely formed.

## Usage

```javascript
<form name="someController.form" novalidate ng-submit="someController.submit()">
  <input name="field" ng-pattern="[0-9]+" required type="text" />
  <mgr-validation form="someController.form" field="someController.form.field" validators="someController.validators.field"></mgr-validation>
  <button type="submit">Submit!</button>
</form>
```

## Available parameters

- `form` - reference to the form, used in a controller
- `field` - reference to the field, used in a controller
- `validators` - an array of validatiors, also used in a controller

Validators are simple objects that look somewhat like this:

```javascript
var someValidator = {
  message: 'You are doing it wrong',
  type: 'invalid', // can be 'invalid' (default), 'valid', and 'warning'
  rule: function (form, field) { return field.$invalid; }
};
```


## Controller

A bit more detailed example in a controller:

```javascript
// Define the validators object
someController.validators = {};

// Define an array of validators for the 'field' field
someController.validators.field = [
  {
    // Show this message on attempt to submit the form with no value in the field
    message: 'Please fill in the field.',
    rule: function (form, field) { return form.$submitted && field.$error.required; }
  },
  {
    // Make sure the field contains a number
    message: 'The field must contain a number.',
    rule: function (form, field) { return field.$error.pattern; }
  },
  {
    // The field is valid, congratulate the user
    message: 'Yay, a valid value!',
    type: 'valid',
    rule: function (form, field) { return field.$valid; }
  }
];
```

For proper examples see demo/.

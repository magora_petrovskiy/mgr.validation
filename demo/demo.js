(function () {
	// jshint latedef: false
	'use strict';

	angular
		.module('demo', [
			'mgr.validation'
		])
		.directive('demo', [
			demo
		]);

	function demo() {
		return {
			restrict: 'AE',
			scope: true,
			controller: DemoController,
			controllerAs: 'demo'
		};

		function DemoController() {
			var demo = this;
		}
	}

})();
